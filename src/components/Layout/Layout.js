import Grid from '@material-ui/core/Grid';
import Login from '../Login/LoginPage';
import MainPanel from '../MainPanel/mainPanelContainer';
import NavBar from '../NavBarPanel/NavBarPanel.js';
import Logout from '../Logout/Logout';
import { Switch, Route, Redirect ,BrowserRouter} from 'react-router-dom';

import React, { Component } from 'react'
import { withStyles  } from '@material-ui/core';


class Layout extends Component {




  render() {
    const classes = this.props.classes;

    return (
      <div >
      <Grid container>
        <BrowserRouter>
          <Switch>
            <Route path='/login' component={() => <Login />} />
            <Route path='/home' component={()=><MainPanel/>}/>
            <Route path='/logout' component={() => <Logout />}/>
            <Redirect to='/login'/>
          </Switch>      
        </BrowserRouter>



      </Grid>
      </div>
    );
  }

}
const styles = theme => ({


})

export default withStyles(styles)(Layout);
