import React, { Component } from 'react'
import { withStyles } from '@material-ui/core';
import {  Redirect } from 'react-router-dom';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import {Button} from '@material-ui/core';
import Image from './game.jpg'; // Import using relative path




class Login extends Component {
    constructor (props) {
        super(props);
        this.state = {
          username:'',
          password:'',
          loggedIn:null,
        };
    
        
      }

      onChange=(event)=>{
        const target = event.target;
        const value = target.value;
        const name = target.name;
    
        this.setState({[name]:value});
    
      }

      handleOnClick=()=>{
        localStorage.setItem("token1","asjhfalsjhf");
        this.setState({loggedIn :true});
      
      }

  

  render() {
    if (this.state.loggedIn){
        return <Redirect push to="/home" />;
      }
    const classes = this.props.classes;

    return (
        <Paper className={classes.root}>

            <Card className={classes.card}>
                <CardHeader style={{textAlign:'center'}}
                    
                    title="Login"
                />
                
                <CardContent >
                  <div style={{marginLeft:'20%'}}>
                <form className="a">
                    <div >
                    <TextField
                            id="standard-name"
                            label="Username"
                            name="username"
                            value={this.state.username}
                            onChange={this.onChange}
                            margin="normal"
                        />
                    </div>
                    <div class="form-group">
                    <TextField
                            id="standard-name"
                            label="Password"
                            name="password"
                            value={this.state.password}
                            onChange={this.onChange}
                            margin="normal"
                        />
                    </div>

                </form>
                    </div>
                </CardContent>
                <CardActions disableSpacing>
                    <IconButton aria-label="add to favorites" style={{marginLeft:'30%'}}>
                    <Button color="primary" type="submit" class="btn btn-primary" onClick={this.handleOnClick}>Submit</Button>
                    </IconButton>
                    
                    
                </CardActions>
               
                </Card>
 
        </Paper>
    );

  }

}
const styles = theme => ({
    card:{
        width:300,
        height:400,
        borderRadius:10,
        boxShadow:'none',
        marginTop:'20%',
        margin:'auto'
    },
    root:{
        width:"100%",
        backgroundImage: `url(${Image})`,
        height:'100vh',
        backgroundSize: 'cover'
    },
    a:{
      marginLeft:'20%'
    }
  


})

export default withStyles(styles)(Login);
