import React, { Component } from 'react'
import { withStyles } from '@material-ui/core';
import {Link , Redirect} from 'react-router-dom';



class Logout extends Component {
    constructor (props) {
        super(props);
        localStorage.removeItem("token1")
        this.state = {
            loggedIn:null
         
        };
      }

  render() {
    if(this.state.loggedIn==false){
        return <Redirect to='/' />
    }
    const classes = this.props.classes;

    return (
        <div>
            <h1>you have been logged out</h1>
            <Link to='/'>Login Again</Link>

        </div>
    );

  }

}
const styles = theme => ({
    


})

export default withStyles(styles)(Logout);
