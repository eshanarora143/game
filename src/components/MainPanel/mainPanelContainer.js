import { connect } from 'react-redux';
import MainPanel from './MainPanel';
import { fetchData } from '../../reducers/rootReducer';


export default connect(
    state => ({

     data : state

   }),
   {
     fetchData
   }


 )(MainPanel)
