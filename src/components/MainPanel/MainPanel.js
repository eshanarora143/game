import React, { Component } from 'react'
import { withStyles,
   Button ,CircularProgress ,
   Grid,Card,CardHeader,CardContent,Avatar,LinearProgress,Container,Paper} from '@material-ui/core';
import classnames from 'classnames';


import {Link,Redirect} from 'react-router-dom'


const BorderLinearProgress = withStyles({
  root: {
    height: 10,
    backgroundColor: ('red', 0.3),
    width:180,
    borderRadius:20
  },
  bar: {
    borderRadius: 20,
    backgroundColor: 'red',
  },
})(LinearProgress);



class MainPanel extends Component {
  constructor(props) {
		super(props);
		this.state = {
        
		}
  }

  componentDidMount(){
    this.props.fetchData()
    

  }
  date=(x)=>{
    var s = new Date(x).toLocaleDateString("en-US")
    return s
  }
  time=(x)=>{
    var s = new Date(x).toLocaleTimeString("en-US")
    return s
  }

  progressBar=(currentPlayers,totalPlayers)=>{
    var a = (currentPlayers/totalPlayers)*100;
    return a

  }
  

  render() {
    console.log(this.props)
    const classes = this.props.classes;

    const fixedHeightPaper = classnames(classes.paper, classes.fixedHeight);
    console.log(localStorage.getItem('token1'))

    if (localStorage.getItem('token1')==null ){
      return <Redirect push to="/login" />;
    }
    


    return (
      <div>
        {
          this.props.data?
          <Grid xs={12} className={classes.mainPanel}>
          <div >
            <Paper className={classes.root}>
              <main className={classes.content}>
                <Container maxWidth="lg" className={classes.container}>
                  <Grid container spacing={3} justify="space-between">
                  
                    {
  
                      this.props.data.data.map((game,index)=>
                      <div>
  
             <Grid item xs={12} md={3} lg={5} key={index} className={classes.grid}>
  
             <Card className={fixedHeightPaper}>
                <CardHeader
                  avatar={
                    <Avatar aria-label="recipe" src={game.arenaLogo} className={classes.bigAvatar}>
                      R
                    </Avatar>
                  }
                  
                  title={game.name}
                  subheader=
                  
                  {
                    <div style={{fontSize:10}}>
                      {
                    this.date(game.startTimestamp)+'('+this.time(game.startTimestamp)+')' +' to ' + this.date(game.endTimestamp)+'('+this.time(game.endTimestamp)+')'
                      }
                    </div>
                  } 
                />
                
                <CardContent>
                  <Grid container>
                    <Grid xs={12} container>
                      <Grid xs={4}>
                        <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                        Game
                        </div>
                        <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                        {game.gameName}
  
                        </div>
                      </Grid>
                      <Grid xs={4}>
                      <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                        Total Prize
                        </div>
                        <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                        Rs.{game.totalWinnings}
  
                        </div>
                      </Grid>
                      <Grid xs={4}>
                      <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                        Entry Fee
                        </div>
                        <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                        Rs.{game.entryFee}
  
                        </div>
                      </Grid>
                    </Grid>
                    <Grid xs={12} container>
                      <Grid xs={9} style={{marginTop:35}}>
                      <BorderLinearProgress
                              className={classes.margin}
                              variant="determinate"
                              color="secondary"
                              value={this.progressBar(game.currentPlayers,game.totalPlayers)}
                            />
                         <div style={{fontSize:13,color:"grey"}}>
                           {game.totalPlayers-game.currentPlayers} places left   {game.currentPlayers} have joined
                         </div>   
                      </Grid>
                      <Grid xs={3} style={{marginTop:20,color:'primary'}}>
                        <Button color='primary'>Join</Button>
                      </Grid>
                    </Grid>
                  </Grid>
                  
                </CardContent>
                
                
              </Card>
             </Grid>
                      </div>)
                      
  
                    }
  
  
  
                  </Grid>
                  <Link to='/logout'>
                  <Button style={{margin:'auto'}} >
                    Log out
                  </Button>
                  </Link>
  
                </Container>
              </main>
            </Paper>
          </div>
        </Grid>        
        :
            <div className={classes.CircularProgress}>
                    <CircularProgress/>
            </div>

        }
       <Grid xs={12} className={classes.mainPanel}>
        <div >
          <Paper className={classes.root}>
            <main className={classes.content}>
              <Container maxWidth="lg" className={classes.container}>
                <Grid container spacing={3} justify="space-between">
                
                  {

                    this.props.data.data.map((game,index)=>
                    <div>

                <Grid item xs={12} md={3} lg={5} key={index} className={classes.grid}>

                <Card className={fixedHeightPaper}>
                    <CardHeader
                      avatar={
                        <Avatar aria-label="recipe" src={game.arenaLogo} className={classes.bigAvatar}>
                          R
                        </Avatar>
                      }
                      
                      title={game.name}
                      subheader=
                      
                      {
                        <div style={{fontSize:10}}>
                          {
                        this.date(game.startTimestamp)+'('+this.time(game.startTimestamp)+')' +' to ' + this.date(game.endTimestamp)+'('+this.time(game.endTimestamp)+')'
                          }
                        </div>
                      } 
                    />
                    
                    <CardContent>
                      <Grid container>
                        <Grid xs={12} container>
                          <Grid xs={4}>
                            <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                            Game
                            </div>
                            <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                            {game.gameName}

                            </div>
                          </Grid>
                          <Grid xs={4}>
                          <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                            Total Prize
                            </div>
                            <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                            Rs.{game.totalWinnings}

                            </div>
                          </Grid>
                          <Grid xs={4}>
                          <div style={{fontSize:15,fontStyle:'bold',textAlign:'center'}}>
                            Entry Fee
                            </div>
                            <div style={{fontSize:13,color:'grey',textAlign:'center'}}>
                            Rs.{game.entryFee}

                            </div>
                          </Grid>
                        </Grid>
                        <Grid xs={12} container>
                          <Grid xs={9} style={{marginTop:35}}>
                          <BorderLinearProgress
                                  className={classes.margin}
                                  variant="determinate"
                                  color="secondary"
                                  value={this.progressBar(game.currentPlayers,game.totalPlayers)}
                                />
                            <div style={{fontSize:13,color:"grey"}}>
                              {game.totalPlayers-game.currentPlayers} places left   {game.currentPlayers} have joined
                            </div>   
                          </Grid>
                          <Grid xs={3} style={{marginTop:20,color:'primary'}}>
                            <Button color='primary'>Join</Button>
                          </Grid>
                        </Grid>
                      </Grid>
                
              </CardContent>
              
              
            </Card>
           </Grid>
                    </div>)
                    

                  }



                </Grid>
                <Link to='/logout'>
                <Button style={{margin:'auto'}} >
                  Log out
                </Button>
                </Link>

              </Container>
            </main>
          </Paper>
        </div>
      </Grid>        
      
      </div>
   
    );
  }

}
const styles = theme => ({

  root: {
    minHeight:'100vh',
    backgroundColor:'#eeeeee'

  },
  mainPanel:{
    minHeight:'100vh'

  },
  paper: {
    padding: theme.spacing(2),
    display: 'flex',
    overflow: 'auto',
    flexDirection: 'column',
    borderRadius:10,
  
  },
  fixedHeight: {
    height: 250,
    width:300,
    margin:20,
    padding:10,
    
},

  grid:{
    padding:10
  },
  
  card: {
    maxWidth: 345,
  },
  totalWinnings:{
    color:'black',
    textAlign:'center'
  },
  bigAvatar: {
    margin: 10,
    width: 60,
    height: 60,
  },


})

export default withStyles(styles)(MainPanel);
