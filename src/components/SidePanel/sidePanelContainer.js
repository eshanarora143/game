import { connect } from 'react-redux';
import SidePanel from './SidePanel';
import { setClassIndex } from '../../reducers/rootReducer'


export default connect(
    state => ({

     data : state

   }),
   {
    	setClassIndex,
    }

  )(SidePanel)
