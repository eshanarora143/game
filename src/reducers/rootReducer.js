import axios from 'axios'
export const initialState = {
  isLoading: true,
  data: [],
  error: false,
};
export const START_FETCH = "Fetch/START_FETCH";
export const FETCH_SUCCESS = "Fetch/FETCH_SUCCESS";
export const FETCH_ERROR = "Fetch/FETCH_ERROR";

export const startFetch = () => ({
  type: START_FETCH
});

export const fetchSuccess = (payload) => ({
  type: FETCH_SUCCESS,
  payload
});

export const fetchError = () => ({
  type: FETCH_ERROR
});


export const fetchData = (s) => dispatch => {
  console.log("fetch data called");
  dispatch(startFetch());
  let myheaders = {
    "X-COSMOS-AUTH": 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNTM4NTcwNTg2OTkxIiwicGxhdGZvcm1Db2RlIjoiMSIsInBsYXRmb3JtVXNlcklkIjoiNzY1NjExOTgyMjM4NjAyMjgiLCJleHAiOjE1NjE3MjU5NzF9.hxH-gpgxfr7zk4AFvkdZ2UzhoxpXSUC8LJ7t1NUf1nxwrSiK-RN5jSwGuWlvfjLW171ZelviEaWC54YUjdAaHw',
  }


  fetch('https://app.tryhard.gg/tournament/open', {
      method: "GET",
      headers: myheaders
    })
  .then(res => res.json())
  .then(data=> 
    dispatch(fetchSuccess(data.response))
    )
  .catch(err => {
      dispatch(fetchError());
  });
};



export default function FetchReducer(state = initialState, { type, payload }) {
  switch (type) {
    case START_FETCH:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        data: payload,
        error: null
      };
    case FETCH_ERROR:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    
    default:
      return state;
  }
}

